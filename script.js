const header = document.createElement('header');
header.className = 'header';

const playerScore = document.createElement('p');
playerScore.className = 'score';
playerScore.textContent = 'Player Score: ';

const humanScoreCount = document.createElement('span');
humanScoreCount.id = 'human-score-count';
playerScore.appendChild(humanScoreCount);

const title = document.createElement('h1');
title.className = 'title';
title.textContent = 'WHACK A MOLE';
header.appendChild(title);

const AIScore = document.createElement('p');
AIScore.className = 'score';
AIScore.textContent = 'AI Score: ';

const AIScoreCount = document.createElement('span');
AIScoreCount.id = 'AI-score-count';
AIScore.appendChild(AIScoreCount);

header.appendChild(playerScore);
header.appendChild(AIScore);

const gameBoardDiv = document.createElement('div');
gameBoardDiv.className = 'game-board';

const table = document.createElement('table');
table.className = 'board';

for (let i = 0; i < 10; i++) {
  const row = document.createElement('tr');

  for (let j = 0; j < 10; j++) {
    const cell = document.createElement('td');
    cell.className = 'cell';
    row.appendChild(cell);
  }

  table.appendChild(row);
}

gameBoardDiv.appendChild(table);

const modalStart = document.createElement('div');
modalStart.className = 'modal-start';

const modalTitle = document.createElement('h3');
modalTitle.className = 'modal-title';
modalTitle.textContent = 'Choose game difficulty: ';
modalStart.appendChild(modalTitle);

const difficultiesButtons = document.createElement('div');
difficultiesButtons.className = 'difficulties-buttons';

const difficultyLevels = ['easy', 'normal', 'difficult'];

difficultyLevels.forEach(level => {
  const button = document.createElement('button');
  button.id = level;
  button.className = 'difficulties-buttons-item';
  button.textContent = level.charAt(0).toUpperCase() + level.slice(1);
  difficultiesButtons.appendChild(button);
});

modalStart.appendChild(difficultiesButtons);

const modalEnd = document.createElement('div');
modalEnd.className = 'modal-end';

const finalScore = document.createElement('p');
finalScore.id = 'final-score';
modalEnd.appendChild(finalScore);

const launchGameButton = document.createElement('button');
launchGameButton.id = 'launch-game';
launchGameButton.className = 'launch-game-button';
launchGameButton.textContent = 'Start New Game';
launchGameButton.addEventListener('click', modalGameStart);
modalEnd.appendChild(launchGameButton);

const body = document.querySelector('body');
body.appendChild(header);
body.appendChild(gameBoardDiv);
body.appendChild(modalStart);
body.appendChild(modalEnd);

// ===== //

let score = 0;
let difficulty;

function randomize(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

class Game {
  constructor(difficulty) {
    this.difficulty = difficulty;
    this.score = score;
    this.generatedRandomsArray = [];
  }

  checkRepeats(randomInstances) {
    if (this.generatedRandomsArray.includes(randomInstances)) {
      randomInstances = randomize(100);
      return this.checkRepeats(randomInstances);
    } else {
      this.generatedRandomsArray.push(randomInstances);
      return randomInstances;
    }
  }
}

class Player {
  constructor(score) {
    this._score = score;
  }
  get score() {
    return this._score;
  }
  set score(value) {
    this._score = value;
  }
}

let humanScoreCounter = document.getElementById('human-score-count');
let AIScoreCounter = document.getElementById('AI-score-count');

document.getElementById('launch-game').addEventListener('click', () => {
  location.reload();
}, true);

window.onload = modalGameStart;

function modalGameStart() {
  document.querySelectorAll('.modal-start')[0].style.display = 'flex';

  let difficultyButtons = document.querySelectorAll('.difficulties-buttons-item');
  difficultyButtons.forEach(function(item) {
    item.addEventListener('click', () => {
      document.querySelectorAll('.modal-start')[0].style.display = 'none';
      switch (item.id) {
        case 'easy':
          difficulty = 1500;
          break;
        case 'normal':
          difficulty = 1000;
          break;
        case 'difficult':
          difficulty = 500;
          break;
      }
      launch();
    });
  });
}

function modalGameEnd(humanPlayer, AI) {
  document.querySelectorAll('.modal-end')[0].style.display = 'flex';

  let finalScore = document.getElementById('final-score');
  if (humanPlayer.score > AI.score) {
    finalScore.insertAdjacentText('afterbegin', `Human wins!`);
  } else {
    finalScore.insertAdjacentText('afterbegin', `AI wins!`);
  }
}

function launch() {
  const game = new Game(difficulty);
  const AI = new Player(0);
  const humanPlayer = new Player(0);
  gameProcess(game, humanPlayer, AI);
}

function gameProcess(game, AI, humanPlayer) {
  let playerClicked = false;
  let randomCellArray = randomize(100);
  let passedRepeatChecks = game.checkRepeats(randomCellArray);
  let randomCell = document.querySelectorAll('.cell')[passedRepeatChecks];

  randomCell.style.backgroundColor = 'blue';
  randomCell.addEventListener('click', () => {
    if (randomCell.id !== 'false') {
      randomCell.id = 'false';
      randomCell.style.backgroundColor = 'green';
      playerClicked = true;
      humanPlayer.score++;
      humanScoreCounter.innerText = humanPlayer.score;
      game.score++;
      if (game.score === 50) {
        modalGameEnd(humanPlayer, AI);
      } else {
        gameProcess(game, AI, humanPlayer);
      }
    }
  });

  setTimeout(() => {
    if (!playerClicked) {
      randomCell.id = 'false';
      randomCell.style.backgroundColor = 'red';
      AI.score++;
      AIScoreCounter.innerText = AI.score;
      game.score++;
      if (game.score === 50) {
        modalGameEnd(humanPlayer, AI);
      } else {
        gameProcess(game, AI, humanPlayer);
      }
    }
  }, game.difficulty);
}